FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
COPY target-prod/url-shorter-1.0-SNAPSHOT.jar app.jar
COPY scripts/wait-for-postgresql.sh wait-for-postgresql.sh
RUN chmod 755 wait-for-postgresql.sh
ENTRYPOINT ["./wait-for-postgresql.sh", "db", "java", "-Djava.security.egd=file:/dev/./urandom","-jar", "-Dspring.profiles.active=prod", "/app.jar"]