# URL Shorter

## Description

Most of us are familiar with seeing URLs like bit.ly or t.co on our Twitter or Facebook feeds.
These are examples of shortened URLs, which are a short alias or pointer to a longer page link.
For example, I can send you the shortened URL http://bit.ly/SaaYw5 that will forward you
to a very long Google URL with search results on how to iron a shirt.

### Mandatory Requirements

- Design and implement an API for short URL creation
- Implement forwarding of short URLs to the original ones
- There should be some form of persistent storage
- The application should be distributed as one or more Docker images

### Additional Requirements

- Design and implement an API for gathering different statistics

### Implementation

Mandatory requirements were implemented though there are improvements could be done.
Application is based on Spring Boot, Lombok, Swagger, Dockerfile Maven Plugin.

### Development environment

Development environment requires:
- Java 1.8
- Maven 3.3.9
- PostgreSQL 9.5
- Docker 17.09 & docker-compose 1.9

Optional:
- Lombok plugin for IDEA

#### Using dev profile

Basic step to local run:

1. Create new database and run `${project.folder}/db/schema/url-shorter.sql`. Schema with first link will be created.
2. Setup database credentials and connection string in `${project.folder}/main/resources/application-dev.yml`.
3. Build project.
4. Run `${project.folder}/target/url-shorter-1.0-SNAPSHOT.jar` with VM option `-Dspring.profiles.active=dev`.
5. If you are using SonarQube locally:
`mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=${your.sonar.token}`

#### Using prod profile

To prepare application for production:

1. On a host machine (where you're going to deploy) create a directory for storage.
2. Setup database credentials and connection string in `${project.folder}/main/resources/application-prod.yml`.
3. In `${project.folder}/docker-compose.yml`: setup volume mapping for PotgreSQL image (on line 14) and database credentials.
4. Build an application image using command `sudo mvn clean install dockerfile:build -Pprod`.
5. After first deployment run `${project.folder}/db/schema/url-shorter.sql` manually.

### Known issues

Basic technical improvements should be done according to priority:

1. Setup automate initializing of database on first startup.
2. Improve PostgreSQL waiting script.
3. Add health check for application.
4. Setup Sonar quality gates.