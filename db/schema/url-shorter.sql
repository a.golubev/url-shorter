CREATE SEQUENCE seq_url_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;
ALTER TABLE seq_url_id OWNER TO url_shorter;

CREATE TABLE url (
    id bigint DEFAULT nextval('seq_url_id'::regclass) NOT NULL,
    url character varying(2083),
    code character varying(20)
);
ALTER TABLE url OWNER TO url_shorter;

INSERT INTO url (id, url, code) VALUES (1, 'https://hibernate.atlassian.net/browse/HHH-12368', 'b');

ALTER TABLE ONLY url
    ADD CONSTRAINT url_id_key UNIQUE (id);