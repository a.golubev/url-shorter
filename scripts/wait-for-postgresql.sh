#!/bin/sh
# wait-for-postgresql.sh

set -e

host="$1"
shift
cmd="$@"

echo "Waiting for postgres 15 sec..."
sleep 15

echo $cmd
exec $cmd