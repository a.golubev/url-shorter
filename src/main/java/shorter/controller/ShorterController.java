package shorter.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import shorter.model.ShorterResponse;
import shorter.service.RedirectService;
import shorter.service.ShorterService;

import static shorter.controller.TechnicalController.NOT_FOUND;
import static shorter.controller.TechnicalController.ROOT;
import static shorter.service.ShorterService.isValidUrl;

@RestController
@Api
public class ShorterController {

    private static final String SHORTEN_PATH = ROOT + "api/shorten";
    private static final String SHORT_URL_REDIRECT = ROOT + "{code}";

    @Autowired
    private ShorterService shorterService;

    @Autowired
    private RedirectService redirectService;

    @PostMapping(path = SHORTEN_PATH)
    public ResponseEntity<ShorterResponse> shorten(String url) {
        if (isValidUrl(url)) {
            ResponseEntity<ShorterResponse> responseEntity =
                    new ResponseEntity<>(new ShorterResponse(), HttpStatus.OK);

            responseEntity.getBody().setSuccess(true);
            responseEntity.getBody().setShorterUrl(shorterService.shorten(url));

            return responseEntity;
        } else {
            ResponseEntity<ShorterResponse> responseEntity =
                    new ResponseEntity<>(new ShorterResponse(), HttpStatus.BAD_REQUEST);

            responseEntity.getBody().getMessages().add("Wrong url format!");

            return responseEntity;
        }
    }

    @GetMapping(path = SHORT_URL_REDIRECT)
    public RedirectView redirect(@PathVariable String code) {
        String urlToRedirect = redirectService.getUrlByCode(code);
        RedirectView redirectView = new RedirectView();

        redirectView.setUrl(null != urlToRedirect ? urlToRedirect : NOT_FOUND);

        return redirectView;
    }
}
