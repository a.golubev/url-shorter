package shorter.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class TechnicalController {

    static final String ROOT = "/";
    static final String NOT_FOUND = ROOT + "redirect/url-not-found";
    private static final String SWAGGER = ROOT + "documentation/swagger-ui.html";

    @GetMapping(path = NOT_FOUND)
    public ResponseEntity notFound() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = ROOT)
    public RedirectView redirectToSwagger() {
        return new RedirectView(SWAGGER);
    }
}
