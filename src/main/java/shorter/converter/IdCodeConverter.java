package shorter.converter;

public final class IdCodeConverter {

    private static final String ALPHANUM = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private IdCodeConverter() {}

    /**
     * Short code generating method based on converting id from decimal numeral system
     * to 62 based (the number of lowercase and uppercase alphanum characters).
     *
     * @param id
     *        Id of record with link
     */
    public static String generateCodeFromId(long id) {
        if (id == 0) throw new IllegalArgumentException("Argument [id] can not be 0!");

        long num = id;
        String code = "";

        while (num > 0) {
            byte charIndex = (byte) (num % 62);
            code = String.valueOf(ALPHANUM.charAt(charIndex)).concat(code);
            num = num / 62;
        }

        return code;
    }

    /*
     * This method returns the id of a record with link information in database.
     * It is reverse algorithm for generateCodeFromId() method.
     *
     * @param code
     *        Short link code
     *
     * public static int getIdFromCode(String code) {
     *     int id = 0;
     *
     *     for (int i = 0; i < code.length(); i ++) {
     *         id += Math.pow(62, code.length() - i - 1) * ALPHANUM.indexOf(code.charAt(i));
     *     }
     *
     *     return id;
     * }
     *
     */
}
