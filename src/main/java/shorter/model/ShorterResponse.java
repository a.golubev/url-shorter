package shorter.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class ShorterResponse {
    private ArrayList<String> messages;
    private boolean success;
    private ShorterUrl shorterUrl;

    public ShorterResponse() {
        messages = new ArrayList<>();
        success = false;
        shorterUrl = null;
    }
}
