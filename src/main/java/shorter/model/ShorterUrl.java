package shorter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "URL")
public class ShorterUrl {

    @Id
    @SequenceGenerator(name="seq_url_id",sequenceName="SEQ_URL_ID", allocationSize=1)
    @GeneratedValue(generator="seq_url_id")
    private long id;

    @Column(name = "URL")
    @NotNull
    private String url;

    @Column(name = "CODE")
    private String code;
}
