package shorter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shorter.model.ShorterUrl;

public interface ShorterUrlRepository extends JpaRepository<ShorterUrl, Long> {
    ShorterUrl findByCode(String code);
}
