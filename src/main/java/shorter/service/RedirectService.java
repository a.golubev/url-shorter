package shorter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shorter.model.ShorterUrl;
import shorter.repository.ShorterUrlRepository;

@Component
public class RedirectService {

    @Autowired
    private ShorterUrlRepository shorterUrlRepository;

    public String getUrlByCode(String code) {
        ShorterUrl shorterUrl = shorterUrlRepository.findByCode(code);

        return null != shorterUrl ? shorterUrl.getUrl() : null;
    }
}
