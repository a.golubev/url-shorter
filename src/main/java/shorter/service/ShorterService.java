package shorter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shorter.model.ShorterUrl;
import shorter.repository.ShorterUrlRepository;

import java.net.MalformedURLException;
import java.net.URL;

import static shorter.converter.IdCodeConverter.generateCodeFromId;

@Component
public class ShorterService {

    @Autowired
    private ShorterUrlRepository shorterUrlRepository;

    public ShorterUrl shorten(String url) {
        ShorterUrl shorterUrl = new ShorterUrl();

        shorterUrl.setUrl(url);
        shorterUrlRepository.save(shorterUrl);

        shorterUrl.setCode(generateCodeFromId(shorterUrl.getId()));
        shorterUrlRepository.save(shorterUrl);

        return shorterUrl;
    }

    public static boolean isValidUrl(String inUrl) {
        try {
            URL url = new URL(inUrl);

            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }
}
