package shorter.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.view.RedirectView;
import shorter.model.ShorterResponse;
import shorter.model.ShorterUrl;
import shorter.service.RedirectService;
import shorter.service.ShorterService;

import static org.mockito.Mockito.when;
import static shorter.controller.TechnicalController.NOT_FOUND;

@RunWith(MockitoJUnitRunner.class)
public class ShorterControllerTest {

    @Mock
    private ShorterService shorterService;

    @Mock
    private RedirectService redirectService;

    @InjectMocks
    private ShorterController testee;

    @Test
    public void shortenTestPositive() {
        String url = "https://google.com/maps";

        ShorterUrl shorterUrl = new ShorterUrl(62L, "https://google.com/maps", "ba");

        when(shorterService.shorten(url)).thenReturn(shorterUrl);

        ResponseEntity<ShorterResponse> result = testee.shorten(url);

        Assert.assertNotNull(result.getBody());
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assert.assertTrue(result.getBody().isSuccess());
        Assert.assertEquals(shorterUrl, result.getBody().getShorterUrl());
        Assert.assertEquals(0, result.getBody().getMessages().size());
    }

    @Test
    public void shortenTestNegative() {
        String url = "javascript:alert('Hello!')";

        ResponseEntity<ShorterResponse> result = testee.shorten(url);

        Assert.assertNotNull(result.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        Assert.assertFalse(result.getBody().isSuccess());
        Assert.assertNull(result.getBody().getShorterUrl());
        Assert.assertEquals(1, result.getBody().getMessages().size());
        Assert.assertEquals("Wrong url format!", result.getBody().getMessages().get(0));
    }

    @Test
    public void redirectTestPositive() {
        String code = "ba";
        String url = "https://google.com/maps";

        when(redirectService.getUrlByCode(code)).thenReturn("https://google.com/maps");

        RedirectView result = testee.redirect(code);

        Assert.assertEquals(url, result.getUrl());
    }


    @Test
    public void redirectTestNegative() {
        String code = "baZ55";

        when(redirectService.getUrlByCode(code)).thenReturn(null);

        RedirectView result = testee.redirect(code);

        Assert.assertEquals(NOT_FOUND, result.getUrl());
    }
}
