package shorter.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class TechnicalControllerTest {

    @InjectMocks
    private TechnicalController testee;

    @Test
    public void notFoundTest() {
        ResponseEntity result = testee.notFound();

        Assert.assertNull(result.getBody());
        Assert.assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }
}
