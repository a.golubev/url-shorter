package shorter.converter;

import org.junit.Assert;
import org.junit.Test;

import static shorter.converter.IdCodeConverter.generateCodeFromId;

public class IdCodeConverterTest {

    @Test
    public void generateCodeFromIdTestPositive() {
        long id = 125;
        String code = generateCodeFromId(id);

        Assert.assertEquals("cb", code);
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateCodeFromIdTestNegative() {
        generateCodeFromId(0);
    }
}
