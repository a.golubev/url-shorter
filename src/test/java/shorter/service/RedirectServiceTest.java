package shorter.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import shorter.model.ShorterUrl;
import shorter.repository.ShorterUrlRepository;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RedirectServiceTest {

    @Mock
    private ShorterUrlRepository shorterUrlRepository;

    @InjectMocks
    private RedirectService testee;

    @Test
    public void getUrlByCodeTestPositive() {
        String code = "ba";

        ShorterUrl shorterUrl = new ShorterUrl(62L, "https://google.com/maps", "ba");

        when(shorterUrlRepository.findByCode(code)).thenReturn(shorterUrl);

        String result = testee.getUrlByCode(code);

        Assert.assertEquals(shorterUrl.getUrl(), result);
    }

    @Test
    public void getUrlByCodeTestNegative() {
        String code = "baZ55";

        when(shorterUrlRepository.findByCode(code)).thenReturn(null);

        String result = testee.getUrlByCode(code);

        Assert.assertNull(result);
    }
}
