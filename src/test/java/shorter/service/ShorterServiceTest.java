package shorter.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import shorter.model.ShorterUrl;
import shorter.repository.ShorterUrlRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static shorter.service.ShorterService.isValidUrl;

@RunWith(MockitoJUnitRunner.class)
public class ShorterServiceTest {

    @Mock
    private ShorterUrlRepository shorterUrlRepository;

    @InjectMocks
    private ShorterService testee;

    @Test
    public void shortenTest() {
        String url = "https://google.com/maps";

        when(shorterUrlRepository.save(any(ShorterUrl.class))).thenAnswer(invocation -> {
                ShorterUrl shorterUrl = (ShorterUrl) invocation.getArguments()[0];
                shorterUrl.setId(61L);
                return null;
            }
        ).thenReturn(any(ShorterUrl.class));

        ShorterUrl result = testee.shorten(url);

        Assert.assertEquals(61L, result.getId());
        Assert.assertEquals("9", result.getCode());
        Assert.assertEquals(url, result.getUrl());
        verify(shorterUrlRepository, times(2)).save(any(ShorterUrl.class));
    }

    @Test
    public void isValidUrlTestPositive() {
        String url = "https://google.com/maps";

        Assert.assertTrue(isValidUrl(url));
    }

    @Test
    public void isValidUrlTestNegative() {
        String url = "javascript:alert('Hello!')";

        Assert.assertFalse(isValidUrl(url));
    }
}
